#include "TFile.h"
#include "TH1F.h"
#include "RooHist.h"
#include "RooStats/ModelConfig.h"
#include "config.h"
#include <iostream>

using namespace std;

config::config(const char* data_path, int nq2, int niso, int ncomp){
  _data_path=data_path;
  _template_histo_name=new TString[ncomp];
  _N_name=new TString[ncomp];
  _nq2=nq2; 
  _niso=niso; 
 _ncomp=ncomp;
 _Narray=new double[nq2*niso*ncomp];
 _NormArray=new double[nq2*niso*ncomp];
 _WeightArray=new double[nq2*niso*ncomp];
 _CatArray=new int[nq2*niso];
 
 _fixedNArray=new bool[ncomp];
 _fixedWeightArray=new bool[ncomp];
 _splitYields=new bool[ncomp];
 _isWeighted=new bool[ncomp];
 _isExcluded=new bool[ncomp];
 for (int icomp=0;icomp<ncomp;icomp++){
   _fixedNArray[icomp]=false;
   _fixedWeightArray[icomp]=false;
   _splitYields[icomp]=false;
   _isWeighted[icomp]=false;
   _isExcluded[icomp]=false;
 }
 _isToy=false;
}

char* config::get_rootname(int iq2, int iiso, int itoy){
  char *root_name=new char[128];
  if (_isToy){
  
    switch (iiso){
    case liso:
      sprintf(root_name,"%s/histos_mi25_q%d_%d.root",_data_path,iq2+1,itoy);
      break;
    case uiso:
      sprintf(root_name,"%s/histos_m25_q%d_%d.root",_data_path,iq2+1,itoy);
      break;
    default:
      sprintf(root_name,"%s/histos_m25_q%d_%d.root",_data_path,iq2+1,itoy);
    }
    
  }else{  
  switch (iiso){
  case liso:
    sprintf(root_name,"%s/histos_mi25_q%d.root",_data_path,iq2+1);
    break;
  case uiso:
    sprintf(root_name,"%s/histos_m25_q%d.root",_data_path,iq2+1);
    break;
  default:
    sprintf(root_name,"%s/histos_m25_q%d.root",_data_path,iq2+1);
  }
  }
  return root_name;

}


char* config::channel_name(int iq2, int iiso){
  char *sig_name=new char[128];
 switch (iiso){
 case liso:
   sprintf(sig_name,"Lcstmu_SIG_q%d",iq2+1);
   break;
 case uiso:
   sprintf(sig_name,"m_Lcstmu_SIG_q%d",iq2+1);
   break;
 }

 return sig_name;
}

char* config::getHistoName(int iq2, int icomp){
  char *hname=new char[128];
  sprintf(hname,"%s%d",_template_histo_name[icomp].Data(),iq2+1);
  if (icomp==config::wsign) 	strcat(hname,"_x");
  return hname;
}


void
config::splitYields(int icomp){
  _splitYields[icomp]=true;
}

char* config::getNName(int iq2, int iiso, int icomp){
  char *normname=new char[128];
  if (_splitYields[icomp]){
    //split yield for q2 and iso bins for the others mu,comb and ws)
    switch (iiso){
    case liso:
      sprintf(normname,"%s%d",_N_name[icomp].Data(),iq2+1);
      break;
    case uiso:
      sprintf(normname,"m_%s%d",_N_name[icomp].Data(),iq2+1);
      break;
    }
  } else
    strcpy(normname,_N_name[icomp].Data());

  if (icomp==config::wsign){
    strcat(normname,"_x");
  }
  return normname;
}

void config::setNarray(double N, int iiso, int icomp){
  for (int iq2=0;iq2<_nq2;iq2++){
    _Narray[getindex(iq2,iiso,icomp)]=N;
  }
  
}
void config::setNarray(double* array, int iiso, int icomp){
  for (int iq2=0;iq2<_nq2;iq2++){
    _Narray[getindex(iq2,iiso,icomp)]=array[iq2];
  }
  
}

double config::getN(int iq2, int iiso, int icomp){
  return _Narray[getindex(iq2,iiso,icomp)];
}

void config::setNorm(double norm, int iq2, int iiso, int icomp){
  _NormArray[getindex(iq2,iiso,icomp)]=norm;
}

void config::setNorm(int iq2, int iiso, int icomp,int itoy){
  	
	//location of root files with data histograms and mc templates 
  auto root_name=get_rootname(iq2,iiso,itoy);
	auto histo_name=getHistoName(iq2,icomp);
	
	//get norm factors
	TFile* hfile=TFile::Open(root_name);
	TH1F* htemp;
	
	hfile->GetObject(histo_name,htemp); assert(htemp!=NULL);
	double norm_factor=1./htemp->Integral();
	setNorm(norm_factor,iq2,iiso,icomp);
	hfile->Close();
}

double config::getNorm(int iq2, int iiso, int icomp){
  return _NormArray[getindex(iq2,iiso,icomp)];
}

char* config::getNormName(int iq2, int iiso, int icomp){
  int t=0;
  char *name=new char[128];
  sprintf(name,"mcNorm_%s%d",_template_histo_name[icomp].Data(),iq2+1);
  if (icomp==config::wsign) 	sprintf(name,"%s%s",name,"_x");
  if (iiso==config::uiso){
    char temp[128];
    sprintf(temp,"m_%s",name);
    strcpy(name,temp);
  }
  return name;
}

void config::setWeight(double* ar1, double* ar2, int iiso, int icomp){
  for (int iq2=0;iq2<_nq2;iq2++){
    _WeightArray[getindex(iq2,iiso,icomp)]=ar1[iq2]*ar2[iq2];
  }
  _isWeighted[icomp]=true;
}

double config::getWeight(int iq2, int iiso, int icomp){
  return _WeightArray[getindex(iq2,iiso,icomp)];
}

char* config::getWeightName(int iq2, int iiso, int icomp){
  char *name=new char[128];
  sprintf(name,"%s_weight%d",_N_name[icomp].Data(),iq2+1);
  if (icomp==config::wsign){
    char* header=name;
    sprintf(name,"%s%s",header,"_x");
  }
  if (iiso==config::uiso){
    char temp[128];
    sprintf(temp,"m_%s",name);
    strcpy(name,temp);
  }
  return name;
  
}

void config::exclude(int icomp){
  if (icomp<_ncomp)
    _isExcluded[icomp]=true;
}

void config::fixParameters(RooStats::ModelConfig* mc){
  for (int iq2 = 0; iq2<nq2(); iq2++){
    for (int iiso=0;iiso<niso();iiso++){
      for (int icomp =0; icomp < ncomp(); icomp++){
	if (isExcluded(icomp)) continue;
	    
	TString varname=getNormName(iq2,iiso,icomp);
	cout << varname+" = " << 
	  ((RooRealVar*)(mc->GetNuisanceParameters()->find(varname)))->getVal() << endl;
	((RooRealVar*)(mc->GetNuisanceParameters()->find(varname)))->setConstant(kTRUE);

	if (_fixedNArray[icomp]==true){
	  varname=getNName(iq2,iiso,icomp);
	  ((RooRealVar*)(mc->GetNuisanceParameters()->find(varname)))->setConstant(kTRUE);
	}
	
	if (_fixedWeightArray[icomp]==true){
	  varname=getWeightName(iq2,iiso,icomp);
	  ((RooRealVar*)(mc->GetNuisanceParameters()->find(varname)))->setConstant(kTRUE);
	}
      }
    }
  }
}

void config::fixN(int icomp){
  _fixedNArray[icomp]=true;
}


void config::fixWeight(int icomp){
  _fixedWeightArray[icomp]=true;
}

plotter::plotter(HistFactorySimultaneous *model_hf,
		 RooCategory *idx,
		 RooAbsData *data){
  _model_hf=model_hf;
  _MLPBNN_frame=new RooPlot*[10];//10 should not be hard coded
  _pull_frame=new RooPlot*[10];
  _idx=idx;
  _data=data;
}
void
plotter::makeFrames(RooRealVar *x, int icat){
  _MLPBNN_frame[icat] = x->frame(Title(x->getTitle()));
  _pull_frame[icat] = x->frame(Title("pull "+x->getTitle()));  
  _icat=icat;
}
void
plotter::plotComp(const char* components,int icol){
  char index[128];
  sprintf(index,"%d",_icat);
  cout<<"plotting frame "<<_icat<<endl
      <<"slice "<<index<<endl
      <<"components "<<components<<endl;
  _model_hf->plotOn(_MLPBNN_frame[_icat],
		   Slice(*_idx,index),
		   ProjWData(*_idx,*_data),
		   DrawOption("F"),
		   FillColor(icol),
		   Components(components),
		   Range(0.,1.));
}
void
plotter::plotData(){
  char catstring[128];
  sprintf(catstring,"channelCat==%d",_icat);
  _data->plotOn(_MLPBNN_frame[_icat],
	       DataError(RooAbsData::Poisson),
	       Cut(catstring));  
}
void
plotter::plotPull(){
  //plot residuals
  RooHist* hresid1 =_MLPBNN_frame[_icat]->pullHist() ;
  _pull_frame[_icat]->addPlotable(hresid1,"P");
}



TGraphErrors*
DiffBF(config* fitconf, RooFitResult *r, int isample){
  double x[5]={-4.5,2,4,6,8.5};
  double ex[5]={5.5,1,1,1,1.5};
  double y[fitconf->nq2()];
  double ey[fitconf->nq2()];
  for (int iq2=0;iq2<fitconf->nq2();iq2++){
    y[iq2]=0;
    ey[iq2]=0;
    for (int iiso=0;iiso<fitconf->niso();iiso++){
      RooRealVar* realvar;
      char yieldName[128];
      if (fitconf->isNFixed(isample)==false){
	strcpy(yieldName,fitconf->getNName(iq2,iiso,isample));
	realvar=(RooRealVar*)(r->floatParsFinal().find(yieldName));
	assert(realvar!=NULL);
	double tmp_y=realvar->getVal();
	double tmp_ey=realvar->getError();
	if (fitconf->isWeighted(isample)){
	  tmp_y*=fitconf->getWeight(iq2,iiso,isample);
	  tmp_ey*=fitconf->getWeight(iq2,iiso,isample);
	}
	y[iq2]+=tmp_y;
	ey[iq2]+=sqrt(ey[iq2]*ey[iq2]+tmp_ey*tmp_ey);;
      }
      else {
	y[iq2]+=fitconf->getN(iq2,iiso,isample);
	ey[iq2]=0;
      }
    }
  }
  TGraphErrors *gr=new TGraphErrors(fitconf->nq2(),x,y,ex,ey);
  return gr;
}


TGraphErrors*
WeightsGraph(config* fitconf, int isample){
  double x[5]={-4.5,2,4,6,8.5};
  double ex[5]={5.5,1,1,1,2};
  double y[fitconf->nq2()];
  double ey[fitconf->nq2()];
  for (int iq2=0;iq2<fitconf->nq2();iq2++){
    y[iq2]=0;
    ey[iq2]=0;
    for (int iiso=0;iiso<fitconf->niso();iiso++){
      y[iq2]+=fitconf->getWeight(iq2,iiso,isample);
      ey[iq2]=0;
    }
  }
  TGraphErrors *gr=new TGraphErrors(fitconf->nq2(),x,y,ex,ey);
  char tit[128]; sprintf(tit,"%s weights",fitconf->compname(isample));
  gr->SetTitle(tit);
  return gr;
}
