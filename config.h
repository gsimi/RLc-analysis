#include "TGraphErrors.h"
#include "RooFitResult.h"
#include "TString.h"
#include "RooStats/ModelConfig.h"
#include "TAxis.h"

#ifndef rlcconfig
#define rlcconfig

class config{
 private:
  
  const char* _data_path;
  int _nq2; //number of q2 bins
  int _niso; //number of isolation bins
  int _ncomp; //number of components in the pdf
  TString* _template_histo_name; //name of template histograms
  TString* _N_name;//name of yield variables
  // template config params represented as a matrix
  // nq2 x niso x ncom
  double *_Narray;
  double *_NormArray;
  double *_WeightArray;
  int *_CatArray;

  //array ro hold configuration of fixed parameters
  bool* _fixedNArray;
  bool* _fixedWeightArray;
  bool* _splitYields;
  bool* _isWeighted;
  bool* _isExcluded;
  int getindex(int iq2, int iiso, int icomp){
    return iq2 + iiso*_nq2 + icomp*_nq2*_niso;
  }
  bool _isToy; 
 public:
  config(const char* data_path, int nq2=5, int niso=2, int ncomp=5);
  const char* path(){return _data_path;}
  int nq2(){return _nq2;}
  int niso() {return _niso;}
  int ncomp() {return _ncomp;}
  bool isToy(){return _isToy;};
  void setToy(){_isToy=true;}
  
  void setHistoNames(TString *sa){for (int i=0;i<_ncomp;i++) _template_histo_name[i]=sa[i];}
  void setNNames(TString *sa){for (int i=0;i<_ncomp;i++) _N_name[i]=sa[i];}

  const char* compname(int icomp){return _N_name[icomp].Data();}
  char* get_rootname(int iq2, int iiso, int itoy=0);
  char* getHistoName(int iq2, int icomp);
  char* channel_name(int iq2, int iiso);

  enum comp {mu=0, tau=1, ds=2, comb=3, wsign=4};
  enum iso{liso, uiso};

  void setNorm(double norm, int iq2, int iiso, int icomp);
  void setNorm(int iq2, int iiso, int icomp,int itoy=0);
  double getNorm(int iq2, int iiso, int icomp);
  char* getNormName(int iq2, int iiso, int icomp);
  
  void setNarray(double* array, int iiso, int icomp);
  void setNarray(double N, int iiso, int icomp);
  double getN(int iq2, int iiso, int icomp);
  char* getNName(int iq2, int iiso, int icomp);
  
  void setWeight(double* ar1, double* ar2, int iiso, int icomp);
  double getWeight(int iq2, int iiso, int icomp);
  char* getWeightName(int iq2, int iiso, int icomp);

  void exclude(int icomp);
  
  //set and get category index used to identify components of simult fit
  void setICat(int icat, int iq2, int iiso){
    _CatArray[iq2+_nq2*iiso]=icat;
  }
  int getICat(int iq2, int iiso){
    return _CatArray[iq2+_nq2*iiso];
  }

  //fix yields, weights, normalizations
  void fixN(int icomp);
  void fixWeight(int icomp);
  void fixParameters(RooStats::ModelConfig* mc);
  bool isNFixed(int icomp){return _fixedNArray[icomp];}
  bool isWeightFixed(int icomp){return _fixedWeightArray[icomp];}
  bool isWeighted(int icomp){return _isWeighted[icomp];}
  bool isExcluded(int icomp){ bool r=true;
    if (icomp<_ncomp)
      r = _isExcluded[icomp];
    return r;
  }
  
  //split yields by bin
  void splitYields(int icomp);
  
};

#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooCategory.h"
#include "RooAbsData.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

class plotter{

 private:
  HistFactorySimultaneous* _model_hf;
  RooPlot** _MLPBNN_frame;
  RooPlot** _pull_frame;
  int _icat;
  RooCategory *_idx;
  RooAbsData *_data;

 public:
  plotter(HistFactorySimultaneous* model_hf,
	  RooCategory *idx,
	  RooAbsData *data);

  void plotComp(const char* components,int icol);
  void plotData();
  void plotPull();
  RooPlot* getObsFrame(int icat){return _MLPBNN_frame[icat];}
  RooPlot* getPullFrame(int icat){return _pull_frame[icat];}
  void makeFrames(RooRealVar* x, int icat);
  void drawObs(int icat){
    _MLPBNN_frame[icat]->Draw();
    _MLPBNN_frame[icat]->SetTitleOffset(1.6) ; 
    _MLPBNN_frame[icat]->SetTitle("q1");    
    _MLPBNN_frame[icat]->GetYaxis()->SetLabelSize(0.03); 

  }    
  void drawPull(int icat){
    _pull_frame[icat]->Draw();
    _pull_frame[icat]->GetXaxis()->SetTitleSize(0.11);
    _pull_frame[icat]->GetXaxis()->SetTitleOffset(0.87);
    _pull_frame[icat]->GetXaxis()->SetLabelSize(0.1);
    _pull_frame[icat]->GetYaxis()->SetLabelSize(0.1);
  }
};

TGraphErrors*
DiffBF(config* fitconf, RooFitResult *r, int isample);

TGraphErrors*
WeightsGraph(config* fitconf, int isample);

#endif

