#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "TCanvas.h"
#include "readtoy.h"

#include <iostream>
using namespace std;

//read numer of tau generated
void ntaugen(int iiso,int iq2){
  char* fname=new char[255];
  TFile *f;
  TH1F* h;
  TH1F* ntaugen=new TH1F("hn","n xxx generated",1000,0,10000);
 char* hname=new char[255];
  sprintf(hname,"hdata%d",iq2);
  const char* siso=iiso==-1? "i":"";
  for (int itoy=1; itoy<500; itoy++){
    sprintf(fname,"toydata/gen_tau511_ds1225_NOMU/histos_m%s25_q%d_%d.root",siso,iq2,itoy);
    cout<<fname<<endl;
    f=TFile::Open(fname);
    if (f==nullptr) continue;
    h=(TH1F*)f->FindObjectAny(hname);
    ntaugen->Fill(h->GetEntries());
    f->Close();
  }
  ntaugen->Draw();
  ntaugen->Fit("gaus");
  float nsigma=ntaugen->GetRMS()/sqrt(ntaugen->GetMean());
  cout<<"nsigma = "<<nsigma<<endl;
  
}


//read toy results from toy files and collect histograms
//of mean, error, pull distributions
void collect_toy_results() {
  cout<<"debug: start collect"<<endl;  
  TString base_path("./");
  TFile f(base_path+"results/toys/toyresult.root","recreate");
  TTree toy("toy","toy");
  Int_t status; 
  Int_t covqual;
  //average number of events generated in toy generator macro

Float_t Nmu_1 = 4237.7;
Float_t Nmu_1m = 329.327;

Float_t Nmu_2 = 8832.81;
Float_t Nmu_2m = 745.266;

Float_t Nmu_3 = 9091.29;
Float_t Nmu_3m = 667.948;

Float_t Nmu_4 = 6259.22;
Float_t Nmu_4m = 433.59;

Float_t Nmu_5 = 2465.59;
Float_t Nmu_5m = 167.26;

Float_t  Nds;
Float_t  Ntau;
  // TTree toy("toy","toy")
  // t1.Branch("px",&px,"px/F");
  // t1.Branch("py",&py,"py/F");
  // t1.Branch("pz",&pz,"pz/F");
  // t1.Branch("random",&random,"random/D");
  // t1.Branch("ev",&ev,"ev/I")
  RooFitResult* result;
  TH1F *Histo_tau= new TH1F("Histo_tau","Histo_tau",80,0,3500);
  TH1F *HistoErr_tau= new TH1F("HistoErr_tau","HistoErr_tau",80,100,150);
  TH1F *HistoPull_tau= new TH1F("HistoPull_tau","HistoPull_tau",80,-6,6);

  TH1F *Histo_ds= new TH1F("Histo_ds","Histo_ds",80, 500,2000);
  TH1F *HistoErr_ds= new TH1F("HistoErr_ds","HistoErr_ds",80,110,180);
  TH1F *HistoPull_ds= new TH1F("HistoPull_ds","HistoPull_ds",80,-6,6);

  TH1F *Histo_mu1= new TH1F("Histo_mu1","Histo_mu1",80, 500,10000);
  TH1F *HistoErr_mu1= new TH1F("HistoErr_mu1","HistoErr_mu1",80,120,200);
  TH1F *HistoPull_mu1= new TH1F("HistoPull_mu1","HistoPull_mu1",80,-6,6);

  TH1F *Histo_mu2= new TH1F("Histo_mu2","Histo_mu2",80, 500,10000);
  TH1F *HistoErr_mu2= new TH1F("HistoErr_mu2","HistoErr_mu2",80,120,200);
  TH1F *HistoPull_mu2= new TH1F("HistoPull_mu2","HistoPull_mu2",80,-6,6);

  TH1F *Histo_mu3= new TH1F("Histo_mu3","Histo_mu3",80, 500,10000);
  TH1F *HistoErr_mu3= new TH1F("HistoErr_mu3","HistoErr_mu3",80,120,200);
  TH1F *HistoPull_mu3= new TH1F("HistoPull_mu3","HistoPull_mu3",80,-6,6);

  TH1F *Histo_mu4= new TH1F("Histo_mu4","Histo_mu4",80, 500,10000);
  TH1F *HistoErr_mu4= new TH1F("HistoErr_mu4","HistoErr_mu4",80,120,200);
  TH1F *HistoPull_mu4= new TH1F("HistoPull_mu4","HistoPull_mu4",80,-6,6);

  TH1F *Histo_mu5= new TH1F("Histo_mu5","Histo_mu5",80, 500,20000);
  TH1F *HistoErr_mu5= new TH1F("HistoErr_mu5","HistoErr_mu5",80,0,200);
  TH1F *HistoPull_mu5= new TH1F("HistoPull_mu5","HistoPull_mu5",80,-6,6);


  TH1F *m_Histo_mu1= new TH1F("m_Histo_mu1","m_Histo_mu1",80, 0,2000);
  TH1F *m_HistoErr_mu1= new TH1F("m_HistoErr_mu1","m_HistoErr_mu1",80,20,100);
  TH1F *m_HistoPull_mu1= new TH1F("m_HistoPull_mu1","m_HistoPull_mu1",80,-6,6);

  TH1F *m_Histo_mu2= new TH1F("m_Histo_mu2","m_Histo_mu2",80, 0,2000);
  TH1F *m_HistoErr_mu2= new TH1F("m_HistoErr_mu2","m_HistoErr_mu2",80,20,100);
  TH1F *m_HistoPull_mu2= new TH1F("m_HistoPull_mu2","m_HistoPull_mu2",80,-6,6);

  TH1F *m_Histo_mu3= new TH1F("m_Histo_mu3","m_Histo_mu3",80, 0,2000);
  TH1F *m_HistoErr_mu3= new TH1F("m_HistoErr_mu3","m_HistoErr_mu3",80,20,100);
  TH1F *m_HistoPull_mu3= new TH1F("m_HistoPull_mu3","m_HistoPull_mu3",80,-6,6);

  TH1F *m_Histo_mu4= new TH1F("m_Histo_mu4","m_Histo_mu4",80, 0,2000);
  TH1F *m_HistoErr_mu4= new TH1F("m_HistoErr_mu4","m_HistoErr_mu4",80,20,100);
  TH1F *m_HistoPull_mu4= new TH1F("m_HistoPull_mu4","m_HistoPull_mu4",80,-6,6);

  TH1F *m_Histo_mu5= new TH1F("m_Histo_mu5","m_Histo_mu5",80, 0,2000);
  TH1F *m_HistoErr_mu5= new TH1F("m_HistoErr_mu5","m_HistoErr_mu5",80,20,100);
  TH1F *m_HistoPull_mu5= new TH1F("m_HistoPull_mu5","m_HistoPull_mu5",80,-6,6);

  TFile *fhistos;
  char *file = new char[255];
  int ntoys=500;
  for (int i=1;i<=ntoys;i++)
    {
      cout<<":::::::::::::::::::::::: "<<i<<endl;

      covqual=0;status=0;

      sprintf(file,"%s%s%i%s",base_path.Data(),"/results/RLcst_roofitresult_",i,".root"); 
      fhistos  =new TFile(file);
      assert(fhistos);
      result  = (RooFitResult*)fhistos->Get("Result"); 
      assert(result);
      cout<<"found fit results for toy "<<i<<endl;
      status=   result->status();
      covqual=  result->covQual();
      result->Print();
      cout<<status<<" "<<covqual<<endl;

      /*
	//tau
      Histo_tau->Fill(((RooRealVar*) result->floatParsFinal().find("Ntau"))->getVal());
      HistoErr_tau->Fill(((RooRealVar*)result->floatParsFinal().find("Ntau"))->getError());
      HistoPull_tau->Fill(((((RooRealVar*) result->floatParsFinal().find("Ntau"))->getVal())-511)/(((RooRealVar*) result->floatParsFinal().find("Ntau"))->getError()));

      //ds
      Histo_ds->Fill(((RooRealVar*) result->floatParsFinal().find("NDs"))->getVal());
      HistoErr_ds->Fill(((RooRealVar*)result->floatParsFinal().find("NDs"))->getError());
      HistoPull_ds->Fill(((((RooRealVar*) result->floatParsFinal().find("NDs"))->getVal())-1226)/(((RooRealVar*) result->floatParsFinal().find("NDs"))->getError()));
*/
      
     //mu1
      Histo_mu1->Fill(((RooRealVar*) result->floatParsFinal().find("Nmu1"))->getVal());
      HistoErr_mu1->Fill(((RooRealVar*)result->floatParsFinal().find("Nmu1"))->getError());
      HistoPull_mu1->Fill(((((RooRealVar*) result->floatParsFinal().find("Nmu1"))->getVal())- Nmu_1)/(((RooRealVar*) result->floatParsFinal().find("Nmu1"))->getError()));

    //mu2
      Histo_mu2->Fill(((RooRealVar*) result->floatParsFinal().find("Nmu2"))->getVal());
      HistoErr_mu2->Fill(((RooRealVar*)result->floatParsFinal().find("Nmu2"))->getError());
      HistoPull_mu2->Fill(((((RooRealVar*) result->floatParsFinal().find("Nmu2"))->getVal())-Nmu_2)/(((RooRealVar*) result->floatParsFinal().find("Nmu2"))->getError()));

    //mu3
      Histo_mu3->Fill(((RooRealVar*) result->floatParsFinal().find("Nmu3"))->getVal());
      HistoErr_mu3->Fill(((RooRealVar*)result->floatParsFinal().find("Nmu3"))->getError());
      HistoPull_mu3->Fill(((((RooRealVar*) result->floatParsFinal().find("Nmu3"))->getVal())-Nmu_3)/(((RooRealVar*) result->floatParsFinal().find("Nmu3"))->getError()));

    //mu4
      Histo_mu4->Fill(((RooRealVar*) result->floatParsFinal().find("Nmu4"))->getVal());
      HistoErr_mu4->Fill(((RooRealVar*)result->floatParsFinal().find("Nmu4"))->getError());
      HistoPull_mu4->Fill(((((RooRealVar*) result->floatParsFinal().find("Nmu4"))->getVal())-Nmu_4)/(((RooRealVar*) result->floatParsFinal().find("Nmu4"))->getError()));

    //mu5
      Histo_mu5->Fill(((RooRealVar*) result->floatParsFinal().find("Nmu5"))->getVal());
      HistoErr_mu5->Fill(((RooRealVar*)result->floatParsFinal().find("Nmu5"))->getError());
      HistoPull_mu5->Fill(((((RooRealVar*) result->floatParsFinal().find("Nmu5"))->getVal())-Nmu_5)/(((RooRealVar*) result->floatParsFinal().find("Nmu5"))->getError()));

     //mu1
      m_Histo_mu1->Fill(((RooRealVar*) result->floatParsFinal().find("m_Nmu1"))->getVal());
      m_HistoErr_mu1->Fill(((RooRealVar*)result->floatParsFinal().find("m_Nmu1"))->getError());
      m_HistoPull_mu1->Fill(((((RooRealVar*) result->floatParsFinal().find("m_Nmu1"))->getVal())-Nmu_1m)/(((RooRealVar*) result->floatParsFinal().find("m_Nmu1"))->getError()));

    //mu2
      m_Histo_mu2->Fill(((RooRealVar*) result->floatParsFinal().find("m_Nmu2"))->getVal());
      m_HistoErr_mu2->Fill(((RooRealVar*)result->floatParsFinal().find("m_Nmu2"))->getError());
      m_HistoPull_mu2->Fill(((((RooRealVar*) result->floatParsFinal().find("m_Nmu2"))->getVal())-Nmu_2m)/(((RooRealVar*) result->floatParsFinal().find("m_Nmu2"))->getError()));

    //mu3
      m_Histo_mu3->Fill(((RooRealVar*) result->floatParsFinal().find("m_Nmu3"))->getVal());
      m_HistoErr_mu3->Fill(((RooRealVar*)result->floatParsFinal().find("m_Nmu3"))->getError());
      m_HistoPull_mu3->Fill(((((RooRealVar*) result->floatParsFinal().find("m_Nmu3"))->getVal())-Nmu_3m)/(((RooRealVar*) result->floatParsFinal().find("m_Nmu3"))->getError()));

    //mu4
      m_Histo_mu4->Fill(((RooRealVar*) result->floatParsFinal().find("m_Nmu4"))->getVal());
      m_HistoErr_mu4->Fill(((RooRealVar*)result->floatParsFinal().find("m_Nmu4"))->getError());
      m_HistoPull_mu4->Fill(((((RooRealVar*) result->floatParsFinal().find("m_Nmu4"))->getVal())-Nmu_4m)/(((RooRealVar*) result->floatParsFinal().find("m_Nmu4"))->getError()));

    //mu5
      m_Histo_mu5->Fill(((RooRealVar*) result->floatParsFinal().find("m_Nmu5"))->getVal());
      m_HistoErr_mu5->Fill(((RooRealVar*)result->floatParsFinal().find("m_Nmu5"))->getError());
      m_HistoPull_mu5->Fill(((((RooRealVar*) result->floatParsFinal().find("m_Nmu5"))->getVal())-Nmu_5m)/(((RooRealVar*) result->floatParsFinal().find("m_Nmu5"))->getError()));
    }


  f.cd();
  /*
  TCanvas *c =new TCanvas("c","", 1000, 600);
  c->Divide(3,1);
  c->cd(1);
  Histo_tau->Draw();
  c->cd(2);
  HistoErr_tau->Draw();
  c->cd(3);
  HistoPull_tau->Draw();
  c->Write();

  TCanvas *c1 =new TCanvas("c1","", 1000, 600);
  c1->Divide(3,1);
  c1->cd(1);
  Histo_ds->Draw();
  c1->cd(2);
  HistoErr_ds->Draw();
  c1->cd(3);
  HistoPull_ds->Draw();
  c1->Write();
  */
  TCanvas *c1mu =new TCanvas("c1mu","", 1000, 600);
  c1mu->Divide(3,1);
  c1mu->cd(1);
  Histo_mu1->Draw();
  c1mu->cd(2);
  HistoErr_mu1->Draw();
  c1mu->cd(3);
  HistoPull_mu1->Draw();
  c1mu->Update();
  c1mu->Draw();
  c1mu->Write();

  TCanvas *c2mu =new TCanvas("c2mu","", 1000, 600);
  c2mu->Divide(3,1);
  c2mu->cd(1);
  Histo_mu2->Draw();
  c2mu->cd(2);
  HistoErr_mu2->Draw();
  c2mu->cd(3);
  HistoPull_mu2->Draw();
  c2mu->Write();

  TCanvas *c3mu =new TCanvas("c3mu","", 1000, 600);
  c3mu->Divide(3,1);
  c3mu->cd(1);
  Histo_mu3->Draw();
  c3mu->cd(2);
  HistoErr_mu3->Draw();
  c3mu->cd(3);
  HistoPull_mu3->Draw();
  c3mu->Write();

  TCanvas *c4mu =new TCanvas("c4mu","", 1000, 600);
  c4mu->Divide(3,1);
  c4mu->cd(1);
  Histo_mu4->Draw();
  c4mu->cd(2);
  HistoErr_mu4->Draw();
  c4mu->cd(3);
  HistoPull_mu4->Draw();
  c4mu->Write();

  TCanvas *c5mu =new TCanvas("c5mu","", 1000, 600);
  c5mu->Divide(3,1);
  c5mu->cd(1);
  Histo_mu5->Draw();
  c5mu->cd(2);
  HistoErr_mu5->Draw();
  c5mu->cd(3);
  HistoPull_mu5->Draw();
  c5mu->Write();


  TCanvas *m_c1mu =new TCanvas("m_c1mu","", 1000, 600);
  m_c1mu->Divide(3,1);
  m_c1mu->cd(1);
  m_Histo_mu1->Draw();
  m_c1mu->cd(2);
  m_HistoErr_mu1->Draw();
  m_c1mu->cd(3);
  m_HistoPull_mu1->Draw();
  m_c1mu->Write();

  TCanvas *m_c2mu =new TCanvas("m_c2mu","", 1000, 600);
  m_c2mu->Divide(3,1);
  m_c2mu->cd(1);
  m_Histo_mu2->Draw();
  m_c2mu->cd(2);
  m_HistoErr_mu2->Draw();
  m_c2mu->cd(3);
  m_HistoPull_mu2->Draw();
  m_c2mu->Write();

  TCanvas *m_c3mu =new TCanvas("m_c3mu","", 1000, 600);
  m_c3mu->Divide(3,1);
  m_c3mu->cd(1);
  m_Histo_mu3->Draw();
  m_c3mu->cd(2);
  m_HistoErr_mu3->Draw();
  m_c3mu->cd(3);
  m_HistoPull_mu3->Draw();
  m_c3mu->Write();

  TCanvas *m_c4mu =new TCanvas("m_c4mu","", 1000, 600);
  m_c4mu->Divide(3,1);
  m_c4mu->cd(1);
  m_Histo_mu4->Draw();
  m_c4mu->cd(2);
  m_HistoErr_mu4->Draw();
  m_c4mu->cd(3);
  m_HistoPull_mu4->Draw();
  m_c4mu->Write();

  TCanvas *m_c5mu =new TCanvas("m_c5mu","", 1000, 600);
  m_c5mu->Divide(3,1);
  m_c5mu->cd(1);
  m_Histo_mu5->Draw();
  m_c5mu->cd(2);
  m_HistoErr_mu5->Draw();
  m_c5mu->cd(3);
  m_HistoPull_mu5->Draw();
  m_c5mu->Write();

  f.Write();
  f.Close();

  
}











