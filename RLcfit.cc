#include <iostream>
#include <stdio.h>

#include "TRandom3.h"
#include "TCanvas.h"
#include "TDatime.h"

#include "TStopwatch.h"
#include "TLegend.h"
#include "TIterator.h"
#include "TH2.h"
#include "TLatex.h"
#include "TGraphErrors.h"

#include "RooChi2Var.h"
#include "RooAbsData.h"
#include "RooRealSumPdf.h"
#include "RooPoisson.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooMCStudy.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooHistPdf.h"
#include "RooSimultaneous.h"
#include "RooExtendPdf.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooMsgService.h"
#include "RooParamHistFunc.h"
#include "RooHist.h"
#include "RooRandom.h"
#include "RooPlot.h"

#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/MinNLLTestStat.h"

#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/HistFactoryModelUtils.h"
#include "RooStats/HistFactory/RooBarlowBeestonLL.h"
#include "config.h"
#include "RLcfit.h"
//#define UNBLIND

using namespace std;

TDatime *date = new TDatime();

config*
RLcfit_configuration(){ 
  bool far_initial_values=false; //in Anna's fits this is true, she used the far initial values
 
  /****** configuration ******/
  cout<< "RLcfit: configuring fit"<<endl;
  const int nq2bins=5;
  const int nisobins=2;
  const int ncomp=5;
  char *data_path=new char[128];
  strcpy(data_path,"/home/simi/anna/rlc-test/data/");
  config* fit_config=new config(data_path,nq2bins,nisobins,ncomp);

  /* Names of histograms and yields */

  TString template_histo_name[ncomp];
  template_histo_name[config::mu]=   "hmu";
  template_histo_name[config::tau]=  "htau";
  template_histo_name[config::ds]=   "hds";
  template_histo_name[config::comb]= "hside";
  template_histo_name[config::wsign]="hws_";

  TString N_name[ncomp];
  N_name[config::mu]=     "Nmu";
  N_name[config::tau]=    "Ntau";
  N_name[config::ds]=     "NDs";
  N_name[config::comb]=   "Nside";
  N_name[config::wsign]=  "Nws";

  fit_config->setHistoNames(template_histo_name);
  fit_config->setNNames(N_name);

  /* Simultaneus fit configuration */

  //split mu,wsign and comb yields per bin
  fit_config->splitYields(config::mu);
  fit_config->splitYields(config::comb);
  fit_config->splitYields(config::wsign);  
  //fix ds and tau q2 shape
  fit_config->fixWeight(config::tau);
  fit_config->fixWeight(config::ds);
  //fix combinatorial and wrong sign yields per bin
  fit_config->fixN(config::comb);
  fit_config->fixN(config::wsign);

  /*    bin ISO < 0.25   */

  //initial values
  Double_t tau_init;
  if (far_initial_values==false)
    tau_init=500;
  else
    tau_init=3000;
  Double_t mu_init[nq2bins]    ={2168,    4528,   4655,   3201,    1261};
  Double_t ds_init;
  if (far_initial_values==false)
    ds_init=1200;
  else
    ds_init=2000;
  Double_t ws_fix[nq2bins]     ={54,     7,     22,    23,     21};
  Double_t side_fix[nq2bins]   ={3128,   3444,   4619,  4523,   2537};

  //mc fractions as a function of q2
  Double_t ds_w_fix[nq2bins]   ={0.051,  0.117,  0.477, 0.275,  0.08};
  Double_t tau_w_fix[nq2bins]  ={0.0105, 0.028,  0.201, 0.4632, 0.297};

  //mc fractions as a function of isolation bin
  Double_t  tau_wiso_fix[nq2bins]   ={0.93, 0.93, 0.93, 0.93, 0.93};
  Double_t  ds_wiso_fix[nq2bins]    ={0.76, 0.76, 0.76, 0.76, 0.76};

  /*    bin ISO > 0.25   */

  //initial yield values
  Double_t m_tau_init;
  if (far_initial_values==false)
    m_tau_init=500;
  else
    m_tau_init=3000;
  Double_t m_mu_init[nq2bins]  ={165,    365 ,   331,   218,    84};
  Double_t m_ds_init;
  if (far_initial_values==false)
    m_ds_init=1200;
  else
    m_ds_init=2000;
  Double_t m_ws_fix[nq2bins]   ={38,     14,      41,    23,     34};
  Double_t m_side_fix[nq2bins] ={781,    1349,   1704,  1716,   1099};

  //mc fractions as a function of q2
  Double_t m_ds_w_fix[nq2bins] ={0.037,  0.124,  0.504, 0.268,  0.067};
  Double_t m_tau_w_fix[nq2bins]={0.027,  0.0235, 0.196, 0.459,  0.294};

  //mc fractions as a function of isolation bin
  Double_t  m_tau_wiso_fix[nq2bins] ={0.07, 0.07, 0.07, 0.07, 0.07};
  Double_t  m_ds_wiso_fix[nq2bins]  ={0.24, 0.24, 0.24, 0.24, 0.24};

  //set integral
  fit_config->setNarray(mu_init,config::liso,config::mu);//per q2 bin yield
  fit_config->setNarray(tau_init,config::liso,config::tau);//single yield
  fit_config->setNarray(ds_init,config::liso,config::ds);//single yield
  fit_config->setNarray(side_fix,config::liso,config::comb); 
  fit_config->setNarray(ws_fix,config::liso,config::wsign);

  fit_config->setNarray(m_mu_init,config::uiso,config::mu);
  fit_config->setNarray(m_tau_init,config::uiso,config::tau);
  fit_config->setNarray(m_ds_init,config::uiso,config::ds);
  fit_config->setNarray(m_side_fix,config::uiso,config::comb);  
  fit_config->setNarray(m_ws_fix,config::uiso,config::wsign);

  //set tau and ds weights
  fit_config->setWeight(ds_w_fix,ds_wiso_fix,config::liso,config::ds);
  fit_config->setWeight(tau_w_fix,tau_wiso_fix,config::liso,config::tau);
  fit_config->setWeight(m_ds_w_fix,m_ds_wiso_fix,config::uiso,config::ds);
  fit_config->setWeight(m_tau_w_fix,m_tau_wiso_fix,config::uiso,config::tau);

  //set histogram scale factor for unit normalization
  for (int iq2=0;iq2<fit_config->nq2();iq2++){
    for (int iiso=0;iiso<fit_config->niso();iiso++){
      for (int icomp=0;icomp<fit_config->ncomp();icomp++){
	fit_config->setNorm(iq2,iiso,icomp);
      }
    }
  }

  return fit_config;
}

config*
RLctoy_configuration(int itoy){
  bool far_initial_values=true; //seems to be needed to make toy fits converge

  /****** configuration ******/
  cout<< "RLcfit: configuring fit"<<endl;
  const int nq2bins=5;
  const int nisobins=2;
  const int ncomp=4;
  char *data_path=new char[128];
  strcpy(data_path,"/home/simi/anna/rlc-test/toydata/gen3");
  config* fit_config=new config(data_path,nq2bins,nisobins,ncomp);
  fit_config->setToy();
  /* Names of histograms and yields */

  TString template_histo_name[ncomp];
  template_histo_name[config::mu]=   "hmu";
  template_histo_name[config::tau]=  "htau";
  template_histo_name[config::ds]=   "hds";
  template_histo_name[config::comb]= "hside";
  //  template_histo_name[config::wsign]="hws_";

  TString N_name[ncomp];
  N_name[config::mu]=     "Nmu";
  N_name[config::tau]=    "Ntau";
  N_name[config::ds]=     "NDs";
  N_name[config::comb]=   "Nside";
  //  N_name[config::wsign]=  "Nws";

  fit_config->setHistoNames(template_histo_name);
  fit_config->setNNames(N_name);

  /* Simultaneus fit configuration */

  //split mu,wsign and comb yields per bin
  fit_config->splitYields(config::mu);
  fit_config->splitYields(config::comb);
  //  fit_config->splitYields(config::wsign);  
  //fix ds and tau q2 shape
  fit_config->fixWeight(config::tau);
  fit_config->fixWeight(config::ds);
  //fix combinatorial and wrong sign yields per bin
  fit_config->fixN(config::comb);
  //fit_config->fixN(config::wsign);

  /*    bin ISO < 0.25   */

  //initial values
  Double_t tau_init;
  if (far_initial_values==false)
    tau_init=500;
  else
    tau_init=3000;
  Double_t mu_init[nq2bins]    ={2168,    4528,   4655,   3201,    1261};
  Double_t ds_init;
  if (far_initial_values==false)
    ds_init=1200;
  else
    ds_init=2000;
  //  Double_t ws_fix[nq2bins]     ={54,     7,     22,    23,     21};
  Double_t side_fix[nq2bins]   ={3128,   3444,   4619,  4523,   2537};

  //mc fractions as a function of q2
  Double_t ds_w_fix[nq2bins]   ={0.051,  0.117,  0.477, 0.275,  0.08};
  Double_t tau_w_fix[nq2bins]  ={0.0105, 0.028,  0.201, 0.4632, 0.297};

  //mc fractions as a function of isolation bin
  Double_t  tau_wiso_fix[nq2bins]   ={0.93, 0.93, 0.93, 0.93, 0.93};
  Double_t  ds_wiso_fix[nq2bins]    ={0.76, 0.76, 0.76, 0.76, 0.76};

  /*    bin ISO > 0.25   */

  //initial yield values
  Double_t m_tau_init;
  if (far_initial_values==false)
    m_tau_init=500;
  else
    m_tau_init=3000;
  Double_t m_mu_init[nq2bins]  ={165,    365 ,   331,   218,    84};
  Double_t m_ds_init;
  if (far_initial_values==false)
    m_ds_init=1200;
  else
    m_ds_init=2000;
  //  Double_t m_ws_fix[nq2bins]   ={38,     14,      41,    23,     34};
  Double_t m_side_fix[nq2bins] ={781,    1349,   1704,  1716,   1099};

  //mc fractions as a function of q2
  Double_t m_ds_w_fix[nq2bins] ={0.037,  0.124,  0.504, 0.268,  0.067};
  Double_t m_tau_w_fix[nq2bins]={0.027,  0.0235, 0.196, 0.459,  0.294};

  //mc fractions as a function of isolation bin
  Double_t  m_tau_wiso_fix[nq2bins] ={0.07, 0.07, 0.07, 0.07, 0.07};
  Double_t  m_ds_wiso_fix[nq2bins]  ={0.24, 0.24, 0.24, 0.24, 0.24};

  //set integral
  fit_config->setNarray(mu_init,config::liso,config::mu);//per q2 bin yield
  fit_config->setNarray(tau_init,config::liso,config::tau);//single yield
  fit_config->setNarray(ds_init,config::liso,config::ds);//single yield
  fit_config->setNarray(side_fix,config::liso,config::comb); 
  //fit_config->setNarray(ws_fix,config::liso,config::wsign);

  fit_config->setNarray(m_mu_init,config::uiso,config::mu);
  fit_config->setNarray(m_tau_init,config::uiso,config::tau);
  fit_config->setNarray(m_ds_init,config::uiso,config::ds);
  fit_config->setNarray(m_side_fix,config::uiso,config::comb);  
  //  fit_config->setNarray(m_ws_fix,config::uiso,config::wsign);

  //set tau and ds weights
  fit_config->setWeight(ds_w_fix,ds_wiso_fix,config::liso,config::ds);
  fit_config->setWeight(tau_w_fix,tau_wiso_fix,config::liso,config::tau);
  fit_config->setWeight(m_ds_w_fix,m_ds_wiso_fix,config::uiso,config::ds);
  fit_config->setWeight(m_tau_w_fix,m_tau_wiso_fix,config::uiso,config::tau);

  //set histogram scale factor for unit normalization
  for (int iq2=0;iq2<fit_config->nq2();iq2++){
    for (int iiso=0;iiso<fit_config->niso();iiso++){
      for (int icomp=0;icomp<fit_config->ncomp();icomp++){
	fit_config->setNorm(iq2,iiso,icomp,itoy);
      }
    }
  }

  return fit_config;
}


config*
RLctoytoy_configuration(int itoy){
  bool far_initial_values=true; //seems to be needed to make toy fits converge

  /****** configuration ******/
  cout<< "RLcfit: configuring fit"<<endl;
  const int nq2bins=5;
  const int nisobins=2;
  const int ncomp=1;
  char *data_path=new char[128];
  strcpy(data_path,"/home/simi/anna/rlc-test/toydata/gen_test");
  config* fit_config=new config(data_path,nq2bins,nisobins,ncomp);
  fit_config->setToy();
  /* Names of histograms and yields */

  TString template_histo_name[ncomp];
  template_histo_name[config::mu]=   "hmu";

  TString N_name[ncomp];
  N_name[config::mu]=     "Nmu";

  fit_config->setHistoNames(template_histo_name);
  fit_config->setNNames(N_name);

  /* Simultaneus fit configuration */

  //split mu,wsign and comb yields per bin
  fit_config->splitYields(config::mu);

  /*    bin ISO < 0.25   */

  //initial values
  Double_t tau_init;
  if (far_initial_values==false)
    tau_init=500;
  else
    tau_init=3000;
  Double_t mu_init[nq2bins]    ={2168,    4528,   4655,   3201,    1261};
  Double_t ds_init;
  if (far_initial_values==false)
    ds_init=1200;
  else
    ds_init=2000;
  //  Double_t ws_fix[nq2bins]     ={54,     7,     22,    23,     21};
  Double_t side_fix[nq2bins]   ={3128,   3444,   4619,  4523,   2537};

  //mc fractions as a function of q2
  Double_t ds_w_fix[nq2bins]   ={0.051,  0.117,  0.477, 0.275,  0.08};
  Double_t tau_w_fix[nq2bins]  ={0.0105, 0.028,  0.201, 0.4632, 0.297};

  //mc fractions as a function of isolation bin
  Double_t  tau_wiso_fix[nq2bins]   ={0.93, 0.93, 0.93, 0.93, 0.93};
  Double_t  ds_wiso_fix[nq2bins]    ={0.76, 0.76, 0.76, 0.76, 0.76};

  /*    bin ISO > 0.25   */

  //initial yield values
  Double_t m_tau_init;
  if (far_initial_values==false)
    m_tau_init=500;
  else
    m_tau_init=3000;
  Double_t m_mu_init[nq2bins]  ={165,    365 ,   331,   218,    84};
  Double_t m_ds_init;
  if (far_initial_values==false)
    m_ds_init=1200;
  else
    m_ds_init=2000;
  //  Double_t m_ws_fix[nq2bins]   ={38,     14,      41,    23,     34};
  Double_t m_side_fix[nq2bins] ={781,    1349,   1704,  1716,   1099};

  //mc fractions as a function of q2
  Double_t m_ds_w_fix[nq2bins] ={0.037,  0.124,  0.504, 0.268,  0.067};
  Double_t m_tau_w_fix[nq2bins]={0.027,  0.0235, 0.196, 0.459,  0.294};

  //mc fractions as a function of isolation bin
  Double_t  m_tau_wiso_fix[nq2bins] ={0.07, 0.07, 0.07, 0.07, 0.07};
  Double_t  m_ds_wiso_fix[nq2bins]  ={0.24, 0.24, 0.24, 0.24, 0.24};

  //set integral
  fit_config->setNarray(mu_init,config::liso,config::mu);//per q2 bin yield

  fit_config->setNarray(m_mu_init,config::uiso,config::mu);

  //set histogram scale factor for unit normalization
  for (int iq2=0;iq2<fit_config->nq2();iq2++){
    for (int iiso=0;iiso<fit_config->niso();iiso++){
      for (int icomp=0;icomp<fit_config->ncomp();icomp++){
	fit_config->setNorm(iq2,iiso,icomp,itoy);
      }
    }
  }


  return fit_config;
}


void RLcfit(int itoy, int excluded_component, int forceBB) {
  //gROOT->ProcessLine(" .L config.c+");

  cout<<"RLcfit: START"<<endl<<endl;

  gROOT->ProcessLine(".x lhcbStyle.C");

  config* fit_config;
  if (itoy>0){
    fit_config=RLctoy_configuration(itoy);
    //fit_config=RLctoytoy_configuration(itoy); //hack
  }
  else
    fit_config=RLcfit_configuration();

  if (excluded_component>=0)
    fit_config->exclude(excluded_component);

  
  /**************************/

  TLatex *t=new TLatex();
  t->SetTextAlign(22);
  t->SetTextSize(0.06);
  t->SetTextFont(132);
  gROOT->ProcessLine("gStyle->SetLabelFont(132,\"xyz\");");
  gROOT->ProcessLine("gStyle->SetTitleFont(132,\"xyz\");");
  gROOT->ProcessLine("gStyle->SetTitleFont(132,\"t\");");
  gROOT->ProcessLine("gStyle->SetTitleSize(0.08,\"t\");");
  gROOT->ProcessLine("gStyle->SetTitleY(0.970);");
  RooRandom::randomGenerator()->SetSeed(date->Get()%100000);
  //  cout << date->Get() << endl; //For ToyMC, so I can run multiple copies 
  //with different seeds without recompiling
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING) ; //avoid accidental unblinding!

  TStopwatch sw, sw2;

  using namespace RooFit;
  using namespace RooStats;
  using namespace HistFactory;

  // Many many flags for steering
  /* STEERING OPTIONS */ 

  //  const bool constrainDstst=true;
  const bool useMinos=false;
  // const bool useMuShapeUncerts = false;
  // const bool useTauShapeUncerts = false;
  // const bool useDststShapeUncerts = false;
  // const bool fixshapes = false;
  // const bool fixshapesdstst = false;
  const bool dofit = true; 
  //  const bool toyMC = false;
  //  const bool slowplots = true;
  bool BBon3d = fit_config->isToy()? false :  true; //flag to enable Barlow-Beeston procedure for all histograms.
  if (forceBB>-1){
    BBon3d=forceBB;
    cout<<"Beaston-Barlow forced "<<BBon3d<<endl;
  }
  //Should allow easy comparison of fit errors with and 
  //without the technique. 3d or not is legacy from an old
  //(3+1)d fit configuration
  //  const int numtoys = 1;
  //  const int toysize = 384236;
  // Set the prefix that will appear before
  // all output for this measurement
  RooStats::HistFactory::Measurement meas("RLcst","RLcst");
  meas.SetOutputFilePrefix("results/RLcst");
  meas.SetExportOnly(kTRUE); //Tells histfactory to not run the fit and display
                             //results using its own 
  if (!fit_config->isExcluded(config::tau))
    meas.SetPOI("Ntau"); //parametro di interesse
  else
    if (!fit_config->isExcluded(config::ds))
      meas.SetPOI("NDs"); //parametro di interesse

  //hack
  //  meas.SetPOI("Nmu1"); //parametro di interesse
  
  
  // set the lumi for the measurement.
  // only matters for the data-driven
  // pdfs the way I've set it up. in invfb
  // variable rellumi gives the relative luminosity between the
  // data used to generate the pdfs and the sample
  // we are fitting

  // actually, now this is only used for the misID
  meas.SetLumi(1.0);
  // meas.SetLumiRelErr(0.05);
  cout<<"RLcfit: starting to read histograms"<<endl;
  RooStats::HistFactory::Channel* chan=new RooStats::HistFactory::Channel[fit_config->nq2()*fit_config->niso()];
  int icat=0;
  //NOTE: order of creation of HistFactory::Channel matters
  for (int iiso=0;iiso<fit_config->niso();iiso++){
    for (int iq2=0;iq2<fit_config->nq2();iq2++){
      
      //location of root files with data histograms and mc templates 
      auto root_name=fit_config->get_rootname(iq2,iiso,itoy);
      char *histo_name=new char[128];

      //create channel (a pdf corresponding to a given q2 and iso bin)
      int ichan=iq2+fit_config->nq2()*iiso;
      chan[ichan]=RooStats::HistFactory::Channel(fit_config->channel_name(iq2,iiso));
      //save the progressive index used to identifyt the compnents of the simultaneus fit
      fit_config->setICat(icat,iq2,iiso);
      icat++;
      
      // Add DATA histogram
      sprintf(histo_name,"hdata%d",iq2+1);
      chan[ichan].SetData(histo_name, root_name);
      if (BBon3d==true)
	chan[ichan].SetStatErrorConfig(0.01,"Poisson");
      //for each component of the fit add the  template histogram
      for (int icomp=0;icomp<fit_config->ncomp();icomp++){
	if (fit_config->isExcluded(icomp)) continue;
	
	/* create sample for MC template histograms */
	auto histo_name=fit_config->getHistoName(iq2,icomp);
	char* histfactname=new char[255];
	if (iiso==config::uiso) sprintf(histfactname,"%sm",histo_name);
	else sprintf(histfactname,"%s",histo_name);	  
	cout<<"histfactname "<<histfactname<<endl;
	RooStats::HistFactory::Sample mctemplate(histfactname ,histo_name,root_name);
	//RooStats::HistFactory::Sample mctemplate(histo_name ,histo_name,root_name);

	//add initial value for component yield
	mctemplate.AddNormFactor(fit_config->getNName(iq2,iiso,icomp), 
				 fit_config->getN(iq2,iiso,icomp),
				 1e-3,
				 100e3);
	
	//normalize component to unity
	mctemplate.AddNormFactor(fit_config->getNormName(iq2,iiso,icomp), 
				 fit_config->getNorm(iq2,iiso,icomp), 
				 1e-9, 
				 1.);
	//fix q2 shape for tau and ds
	if (fit_config->isWeightFixed(icomp)){
	  mctemplate.AddNormFactor(fit_config->getWeightName(iq2,iiso,icomp),
				   fit_config->getWeight(iq2,iiso,icomp), 
				   0, 
				   1);
	  if (BBon3d==true)
	    mctemplate.ActivateStatError();

	}
	chan[ichan].AddSample(mctemplate); 
      }
      meas.AddChannel(chan[ichan]);
    }
  }
  
  meas.CollectHistograms();
  RooWorkspace *w;
  w=RooStats::HistFactory::MakeModelAndMeasurementFast(meas);

  ModelConfig *mc = (ModelConfig*) w->obj("ModelConfig"); // Get model manually
  RooSimultaneous *model = (RooSimultaneous*)mc->GetPdf();
  model->Print("V");

    RooRealVar* poi = (RooRealVar*) mc->GetParametersOfInterest()->createIterator()->Next();
    cout<<":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl;
    std::cout << "Param of Interest: " << poi->GetName() << std::endl;  

    // Lets tell roofit the right names for our histogram variables //
    RooArgSet *obs = (RooArgSet*) mc->GetObservables();
    cout<<":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"<<endl;
    obs->Print("v") ;
    cout<<"::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.::"<<endl;
    // For simultaneous fits, this is the category histfactory uses to sort the channels

    //NOTE: channelCat is a progressive index that depends on the order of creation of the channels with the function HistFactory::Channel(..)
    RooCategory *idx = (RooCategory*) obs->find("channelCat");
    RooAbsData *data = (RooAbsData*) w->data("obsData");

    /// 
    cout<<"::::data::::"<<endl;
    data->Print(); 

    //fix parameters of the fit
    fit_config->fixParameters(mc);
    
    RooRealVar *theVar5 = (RooRealVar*)mc->GetNuisanceParameters()->find("Lumi");
    theVar5->setConstant(kTRUE);
  
    HistFactorySimultaneous* model_hf = new HistFactorySimultaneous( *model );

    //  RooFitResult *toyresult;
    RooAbsReal *nll_hf; 

    RooFitResult *result=NULL;//, *result2=NULL;

    cerr << "Saving PDF snapshot" << endl;
    RooArgSet *allpars;
    allpars=(RooArgSet*)((RooArgSet*) mc->GetNuisanceParameters())->Clone();
    allpars->add(*poi);
    RooArgSet *constraints;
    constraints = (RooArgSet*) mc->GetConstraintParameters();
    if(constraints != NULL) allpars->add(*constraints);
    w->saveSnapshot("TMCPARS",*allpars,kTRUE);
    RooRealVar poierror("poierror","poierror",0.00001,0.010);
    //  TIterator *iter = allpars->createIterator();
    //  RooAbsArg *tempvar;
    RooArgSet *theVars = (RooArgSet*) allpars->Clone();
    theVars->add(poierror);
 


  if(dofit)  {//return;

    nll_hf= model_hf->createNLL(*data,RooFit::Extended(kTRUE));

    cout << "***************modello*******************************************************" << endl;
     model_hf->Print("V");
    cout << "***************modello*******************************************************" << endl;
    RooMinuit* minuit_hf = new RooMinuit(*nll_hf) ;
    RooArgSet *temp = new RooArgSet();
    nll_hf->getParameters(temp)->Print("V");
    cout << "**********************************************************************" << endl;

    std::cout << "Minimizing the Minuit (Migrad)" << std::endl;
    //    if(toyMC) {w->loadSnapshot("TMCPARS");}
    //    else {w->saveSnapshot("TMCPARS",*allpars,kTRUE);}

    sw.Reset();
    sw.Start();
    minuit_hf->setStrategy(2);
    minuit_hf->fit("msh");
    RooFitResult *tempResult=minuit_hf->save("TempResult","TempResult");

    cout << tempResult->edm() << endl;
    if (useMinos) minuit_hf->minos(RooArgSet(*poi));
    sw.Stop();
    result = minuit_hf->save("Result","Result");

        
  }

  
  if(result != NULL)
    {
      if (fit_config->isToy()==false  && BBon3d==false){
	printf("Fit ran with status %d\n",result->status());
	printf("EDM at end was %f\n",result->edm());
	cout << "CURRENT NUISANCE PARAMETERS:" << endl;
	TIterator *paramiter = mc->GetNuisanceParameters()->createIterator();
	//TIterator *paramiter = result->floatParsFinal().createIterator();
	RooRealVar *__temp= (RooRealVar *)paramiter->Next();
	int final_par_counter=0;
	while (__temp!=NULL)
	  {
	    if(!__temp->isConstant())
	      {
		if(!(TString(__temp->GetName()).EqualTo(poi->GetName())))
		  {
		    cout << final_par_counter << ": "
			 << __temp->GetName() << "\t\t\t = "
			 << ((RooRealVar*)result->floatParsFinal().find(__temp->GetName()))->getVal()
			 << " +/- "
			 << ((RooRealVar*)result->floatParsFinal().find(__temp->GetName()))->getError() << endl;
		  }
	      }
	    final_par_counter++;
	    __temp=(RooRealVar *)paramiter->Next();
	  }
      }      
      
      result->correlationMatrix().Print();
      
      if (dofit) printf("Stopwatch: fit ran in %f seconds\n",sw.RealTime());
      string fname=meas.GetOutputFilePrefix();
      if (fit_config->isToy()){
	char *rtoy = new char[255];
	sprintf(rtoy,"%s%d%s","_roofitresult_",itoy,".root");  
	fname.append(rtoy);
      }else
	fname.append("_roofitresult.root");

      cout<<"roofitresult file name "<<fname<<endl;
      TFile *f = TFile::Open(fname.c_str(),"RECREATE");
      result->Write();
      f->Write();
      f->Close();
    }


  //
  cout<<"preparing to plot on frame"<<endl;
  
  // RooPlot *MLPBNN_frame[fit_config->nq2()*fit_config->niso()];
  // RooPlot *pull_frame[fit_config->nq2()*fit_config->niso()];
  plotter fit(model_hf,idx,data);
  if (fit_config->isToy()==false){

    for (int iiso=0;iiso<fit_config->niso();iiso++){
      for (int iq2=0;iq2<fit_config->nq2();iq2++){
	cout<<"iiso = "<<iiso<<", iq2 = "<<iq2<<endl;
	
	TString obsname=fit_config->channel_name(iq2,iiso);
	obsname="obs_x_"+obsname;
	cout<<"searching obs "<<obsname<<endl;
	RooRealVar *x = (RooRealVar*) obs->find(obsname);
	assert(x!=NULL);
	char xtit[128]; sprintf(xtit,"MLPBNN q%d",iq2+1);
	x->SetTitle(xtit);

	fit.makeFrames(x,fit_config->getICat(iq2,iiso));
	//plot Data
	fit.plotData();
	//plot Fit
	fit.plotComp("*hmu*,*tau*,*ds*,*side*,*ws*",kBlue);
	//plot residuals
	fit.plotPull();
	//plot fit components
	fit.plotComp("*tau*,*ds*,*side*,*ws*",kViolet);
	fit.plotComp("*tau*,*ds*,*ws*",kGreen);
	fit.plotComp("*ds*,*ws*",kOrange);
	fit.plotComp("*ws*",kRed);
	//replot data on top of everything
	fit.plotData();
      }
    }

    cout<<"preparing to draw plots on canvases"<<endl;

    for (int iiso=0;iiso<2;iiso++){
      char ctit[128];
      switch(iiso){
      case config::uiso:
	sprintf(ctit,"Upper_ISO");
	break;
      case config::liso:
	sprintf(ctit,"Lower_ISO");
	break;
      }

      char cname[128]; sprintf(cname,"c1%d",iiso);
      TCanvas *c1 = new TCanvas(cname,ctit,1200,800);
      //      c1->Divide(3,2); 
      for (int iq2=0;iq2<5;iq2++){
	c1->cd();
	TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
	pad1->SetBottomMargin(0.015);   
	pad1->Draw();   
	pad1->cd(); 
	int icat=fit_config->getICat(iq2,iiso);

	fit.drawObs(icat);
	c1->cd();         
	TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.28);
	pad2->SetTopMargin(0.0);
	pad2->SetBottomMargin(0.2);
	pad2->Draw();
	pad2->cd();
	fit.drawPull(icat);
	TString plotfname;
	plotfname="results/fitted_NN_";
	plotfname+=ctit;
	plotfname+="_q";
	plotfname+=iq2;
	c1->SaveAs(plotfname+".png");
	
	pad1->SetLogy(1);
	pad1->Update();
	c1->SaveAs(plotfname+"_log.png");
      }

    }
    
  // /*
  // TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
  // pad1->SetBottomMargin(0); // Upper and lower plot are joined
  // pad1->Draw();             // Draw the
//   upper pad: pad1
//   pad1->cd();               // pad1 becomes the current pad
//   MLPBNN_frame[1]->Draw();
//   MLPBNN_frame[1]->SetTitleOffset(1.6) ; 
//   MLPBNN_frame[1]->SetTitle("q1");
//   MLPBNN_frame[1] ->SetStats(0);          // No statistics on upper plot
 
//   TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.3);
//   //pad2->SetTopMargin(0.3);
//   pad2->Draw();
//    pad2->SetTopMargin(0.02);
//   pad2->cd();  
//   frame3->Draw();    
// */
  //     c1->SetLogy();

  // const char mu[80] = "#Lambda_{b}#rightarrow#Lambda_{c}^{+}(2625)#mu#nu ";
  // const char tau[80] = "#Lambda_{b}#rightarrow#Lambda_{c}^{+}(2625)#tau#nu ";
  // const char back[500]="#Lambda_{b}#rightarrow#Lambda_{c}^{+}(2625)D_{s} + #Lambda_{b}#rightarrow#Lambda_{c}^{+}(2625)D_{s}^{*}";


  //  TCanvas *c3 = new TCanvas("c3","c3",500,300);

  //  TLegend* leg = new TLegend(0.1, 0.1, .9, .9);
  //  TH1F* h = new TH1F("", "", 1, 0, 1);
  //  TH1* h1 = new TH1F("", "", 1, 0, 1);
  //  TH1* h2 = new TH1F("", "", 1, 0, 1);
  //  TH1* h3 = new TH1F("", "", 1, 0, 1);
  //  TH1* h4 = new TH1F("", "", 1, 0, 1);
  //  TH1* h5 = new TH1F("", "", 1, 0, 1);
 
  // h->SetLineColor(kBlack);
  //  h1->SetFillColor(kBlue);
  //  h2->SetFillColor(kViolet);
  //  h3->SetFillColor(kGreen);
  //  h4->SetFillColor(kOrange);
  //  h5->SetFillColor(kRed);

  //  leg->AddEntry(h, "LHCb Data", "lep"); 
  //  leg->AddEntry(h1, mu, "F");  
  //  leg->AddEntry(h2,"data #Deltam #subset [360,380] " , "F");
  //  leg->AddEntry(h3, tau, "F");  
  //  leg->AddEntry(h4, back, "F");
  //  leg->AddEntry(h5,"WC combination" , "F");  

  //  leg->Draw();
  }
}

 
//mm2q2_frame[i]->pullHist();

	 
TCanvas*
plotDiffYield(const char* resultFile, int isample=config::mu){
  config* fit_config=RLcfit_configuration();
  TGraphErrors *gr;
  TString title;
  TFile *f = TFile::Open(resultFile);
  RooFitResult* result=(RooFitResult*)f->FindObjectAny("Result");
  assert(result!=NULL);
  gr=DiffBF(fit_config,result,isample);
  title=" differential yield";
  title=fit_config->compname(isample)+title;
  TCanvas *c3 = new TCanvas("c3",title,800,800);
  gr->SetTitle(title);
  gr->Draw("AP*");
  return c3;
}

TCanvas*
plotWeights(int isample=config::tau){
  config* fit_config=RLcfit_configuration();
  TGraphErrors *gr;
  gr=WeightsGraph(fit_config,isample);
  TString title;
  title=" weights";
  title=fit_config->compname(isample)+title;
  TCanvas *c3 = new TCanvas("c3",title,800,800);
  gr->SetTitle(title);
  gr->Draw("AP*");
  return c3;
}

  
void
saveDiffYields(char* fresults, char* fplots){
  TCanvas *c3;
  TString ts_fplots(fplots);
  c3=plotDiffYield(fresults,config::mu );
  c3->SaveAs(ts_fplots+"[");
  c3->SaveAs(ts_fplots);
  c3=plotWeights(config::tau);
  c3->SaveAs(ts_fplots);
  c3=plotDiffYield(fresults,config::tau );
  c3->SaveAs(ts_fplots);
  c3=plotWeights(config::ds);
  c3->SaveAs(ts_fplots);
  c3=plotDiffYield(fresults,config::ds );
  c3->SaveAs(ts_fplots);
  c3=plotDiffYield(fresults,config::comb );
  c3->SaveAs(ts_fplots);
  c3=plotDiffYield(fresults,config::wsign );
  c3->SaveAs(ts_fplots);
  c3->SaveAs(ts_fplots+"]");
}
