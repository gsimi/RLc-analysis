#include "config.h"
#include "TCanvas.h"

config*
RLcfit_configuration();

config*
RLctoy_configuration(int itoy);

void
RLcfit(int itoy=0, int excluded_comp=-1, int forceBB=-1);

TCanvas*
plotDiffYield(const char* resultFile, int isample);

TCanvas*
plotWeights(int isample);

void
saveDiffYields(char* fresults, char* fplots);
