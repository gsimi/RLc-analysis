#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class config;
#pragma link C++ class plotter;

#pragma link C++ function RLcfit;
#pragma link C++ function DiffBF;
#pragma link C++ function WeightsGraph;
#pragma link C++ function RLcfit_configuration;
#pragma link C++ function plotDiffYield;
#pragma link C++ function plotWeights;
#pragma link C++ function saveDiffYields;

#pragma link C++ function ntaugen;
#pragma link C++ function collect_toy_results;

#endif
