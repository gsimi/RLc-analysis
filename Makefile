# Library
LIBNAME  := libRLcfit.a
SLIBNAME  := libRLcfit.so

# Compiler
CXX = g++

# Paths
SRC := ./
LIB := lib

# Compiler flags
ROOTCONFIG := root-config
ROOTCLING := rootcling
ROOTCFLAGS := $(shell $(ROOTCONFIG) --cflags)
CXXFLAGS := -Wall -fPIC -O3 -std=c++11 # C++11
CXXFLAGS += $(ROOTCFLAGS)
LDFLAGS := -shared

# Extra libs
ROOTLIBS  := $(shell $(ROOTCONFIG) --libs) 
EXTRALIBS := -l RooStats -l HistFactory

# File type extensions
SrcSuf = cc
HdrSuf = h
ObjSuf = o

# Name of Dictionary for interactive shared library
DICT      := $(SRC)/RLcfitDict.$(SrcSuf)
DICTH     := $(DICT:.$(SrcSuf)=.$(HdrSuf))
DICTO     := $(DICT:.$(SrcSuf)=.$(ObjSuf))
LINKDEF   := $(SRC)/LinkDef.h

# Src, obj, inc
SRCS = $(wildcard $(SRC)/*.$(SrcSuf))
# exclude dictionary from sources
SRCS := $(filter-out $(DICT),$(SRCS))
HEADERS = $(wildcard $(SRC)/*.$(HdrSuf))
# exclude LinkDef from headers
HEADERS := $(filter-out $(LINKDEF),$(HEADERS))
OBJS = $(SRCS:.$(SrcSuf)=.$(ObjSuf))

.PHONY: all clean lib slib 

.DEFAULT: all

all: slib

slib: $(LIB)/$(SLIBNAME) 

lib: $(LIB)/$(LIBNAME)

# Generate Dictionary
$(DICT): $(HEADERS) $(LINKDEF)
	@echo "Generating dictionary $@..."
	$(ROOTCLING) -f $@ -c $^

# Make shared library
$(LIB)/$(SLIBNAME): $(OBJS) $(DICTO)
	@echo "Generating shared library $@..."
	$(CXX) $(CXXFLAGS) $^ $(ROOTLIBS) $(EXTRALIBS) -o $@ $(LDFLAGS)

# Make library
$(LIB)/$(LIBNAME): $(OBJS)
	@echo "Generating static library $@..."
	ar rcs $@ $^

# Make objs
%.$(ObjSuf): %.$(SrcSuf) $(HEADERS)
	@echo "generating object $@..."
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(ROOTLIBS) $(EXTRALIBS)

# Clean
clean:
	rm -f $(OBJS) $(DICTO)


distclean: clean
	rm -f $(LIB)/$(LIBNAME) $(LIB)/$(SLIBNAME) $(DICT) $(DICTH) *.pcm *_$(SrcSuf).d  *_$(SrcSuf).so 
